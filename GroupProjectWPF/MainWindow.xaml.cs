﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace GroupProjectWPF
{
   
    public partial class MainWindow : Window
    {
        string currentFile;
        List<string> database;
        Animal a, buffer;
        int currentPosition;
        
        public MainWindow()
        {
            InitializeComponent();
            database = new List<string>();
            currentPosition = 0;
            lblListChange();
        }

        
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        //Help -> About
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            About frmAbout = new About();
            frmAbout.Show();
        }

        //Help -> Help
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Help frmHelp = new Help();
            frmHelp.Show();
        }
    
        //File -> Open
        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog open = new Microsoft.Win32.OpenFileDialog();
            open.DefaultExt = ".txt";
            open.ShowDialog();
            //retreive the filename
            currentFile = Convert.ToString(open.OpenFile());
            //open file for uploading information
            Stream s1 = open.OpenFile();
            //create reader 
            StreamReader reader = new StreamReader(s1);

            while (!reader.EndOfStream)
            {
                string str = reader.ReadLine();
                database.Add(str);
            }
            
            DisplayObject();
            lblListChange();
        }

        //File -> Close
        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //File -> Save
        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            FileStream f1 = new FileStream(currentFile,
                FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(f1);

            //convert object to string
            string str = a.ObjectToString();
            writer.WriteLine(str);
            writer.Close();
        }

        //File -> SaveAs
        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog save = new Microsoft.Win32.SaveFileDialog();
            save.ShowDialog();
            Stream textOut = save.OpenFile();

            StreamWriter writer = new StreamWriter(textOut);
            string str = a.ObjectToString();
            writer.WriteLine(str);
            writer.Close();
        }


        //Edit Button
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            txtOrder.IsReadOnly = false;
            txtFamily.IsReadOnly = false;
            txtGenus.IsReadOnly = false;
            txtName.IsReadOnly = false;
            txtLength.IsReadOnly = false;
            txtWeight.IsReadOnly = false;
            txtPathToImage.IsReadOnly = false;
        }

        //Save Button
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //a.AnimalClass  = comboBox.something;
            a.Order = txtOrder.Text;
            a.Family = txtFamily.Text;
            a.Genus = txtGenus.Text;
            a.Name = txtName.Text;
            a.Length = txtLength.Text;
            a.Weight = txtWeight.Text;
            a.Conserve = ConserveStatus();
            a.Edited = DateTime.Now;
            lblEdit.Content = "Last Edited: " + a.Edited.ToString();
        }

        //method to take the object in list's properties and assign to a temperary object
        //It will then chnage the fields in the program to their respective values
        public void DisplayObject()
        {
            a = new Animal(database[currentPosition]);

            txtFamily.Text = a.Family;
            txtOrder.Text = a.Order;
            txtGenus.Text = a.Genus;
            txtName.Text = a.Name;
            txtWeight.Text = a.Weight;
            txtLength.Text = a.Length;
            txtPathToImage.Text = a.PathToImage;
            lblCreated.Content = "Created: " + a.Created;
            lblEdit.Content = "Last Edited: " + a.Edited;
          /*  switch (temp.Conserve)
            {
                case "Extinct": rdioExtinct.Checked = true;
                    break;
                case "Threatened": rdioThreatned.IsEnabled = true;
                    break;
                case "Least": rdioLeast.IsEnabled = true;
                    break;
                default:
                    break;
            }
            */
        }

        //Previous Button
        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (currentPosition > 0)
            {
                currentPosition--;
                DisplayObject();
            }
            else
                DisplayObject();
            lblListChange();

        }

        //Next Button
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (currentPosition < database.Count)
            {
                currentPosition++;
                DisplayObject();
            }
            else
                DisplayObject();
            lblListChange();
        }
        
        //Method to return conservation status from radio buttons
        public string ConserveStatus()
        {
            string str;
           if (rdioExtinct.IsEnabled)
                str = "Extinct";
            else if (rdioThreatned.IsEnabled)
                str = "Threatened ";
            else
                str = "Least";

            return str;

        }

        //Set image button
        private void btnSetImage_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog open = new Microsoft.Win32.OpenFileDialog();
            open.DefaultExt = ".jpg";
            open.ShowDialog();
            string path = Convert.ToString(open.OpenFile());
            txtPathToImage.Text = path;
            //image.Source = new BitmapImage(new Uri(path));
            
        }

        //Method to display curent postition in the list database
        public void lblListChange()
        {
            lblList.Content = "Animal (" + (currentPosition + 1) + " of " + database.Count + ")";
        }
    }
}
