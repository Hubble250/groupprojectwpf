﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GroupProjectWPF
{
    class Animal
    {
        //Properties
        private string animalClass;
        private string order;
        private string family;
        private string genus;
        private string name;
        private string weight;
        private string length;
        private string conserve;
        private string pathToImage;
        private DateTime edit;
        private DateTime created;

        //default constructor
        public Animal()
        {
            animalClass = "";
            order = "";
            family = "";
            genus = "";
            name = "";
            weight = "";
            length = "";
            conserve = "";
            pathToImage = "";
            edit = DateTime.Now;
            created = DateTime.Now;

        }

        //contructor to create object with all possible parameters
        public Animal(string c, string o, string f, string g, string n, string w, string l, string con)
        {
            animalClass = c;
            order = o;
            family = f;
            genus = g;
            name = n;
            weight = w;
            length = l;
            conserve = con;

        }

        //create object from a single string
        public Animal(string str)
        {
            ReadString(str);
        }

        //read a string and convert substrings to object properties
        public void ReadString(string str)
        {
            string[] input = str.Split('|');
            animalClass = input[0];
            order = input[1];
            family = input[2];
            genus = input[3];
            name = input[4];
            weight = input[5];
            length = input[6];
            conserve = input[7];
            pathToImage = input[8];
            edit = DateTime.Parse(input[9]);
            created = DateTime.Parse(input[10]);
        }


        //Methods to retrieve and set properties of object

        public string AnimalClass
        {
            get
            {
                return animalClass;
            }

            set
            {
                animalClass = value;
            }
        }

        public string Order
        {
            get
            {
                return order;
            }

            set
            {
                order = value;
            }
        }

        public string Family
        {
            get
            {
                return family;
            }

            set
            {
                family = value;
            }
        }

        public string Genus
        {
            get
            {
                return genus;
            }

            set
            {
                genus = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Weight
        {
            get
            {
                return weight;
            }

            set
            {
                weight = value;
            }
        }

        public string Length
        {
            get
            {
                return length;
            }

            set
            {
                length = value;
            }
        }

        public string Conserve
        {
            get
            {
                return conserve;
            }

            set
            {
                conserve = value;
            }
        }

        public string PathToImage
        {
            get
            {
                return pathToImage;
            }

            set
            {
                pathToImage = value;
            }
        }

        public DateTime Edited
        {
            get
            {
                return edit;
            }

            set
            {
                edit = value;
            }
        }

        public DateTime Created
        {
            get
            {
                return created;
            }

            set
            {
                created = value;
            }
        }

        //method to convert the object to a string
        public string ObjectToString()
        {
            string str = "";
            str += animalClass + "|";
            str += order + "|";
            str += family + "|";
            str += genus + "|";
            str += name + "|";
            str += weight + "|";
            str += length + "|";
            str += conserve + "|";
            str += pathToImage + "|";
            str += Convert.ToString(edit) + "|";
            str += Convert.ToString(created) + "|";

            return str;
        }
    }
}
